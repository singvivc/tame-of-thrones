package com.tameofthrones.app.chainbreaker;

import com.tameofthrones.app.chainbreaker.model.Priest;
import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChainBreakerTest {

  private Priest priest;
  private ChainBreaker chainBreaker;
  private Universe universe = Universe.createUniverse();

  @Before
  public void before() {
    priest = mock(Priest.class);
    this.chainBreaker = new ChainBreaker(priest);
  }

  @After
  public void after() {
    universe.getKingdoms().stream().forEach(kingdom -> kingdom.getAllies().clear());
  }

  @Test
  public void givenCompetingKingdoms_testShouldReturnTheRulerOfSoutheros() {
    List<String> competingKingdoms = List.of("Air", "Ice", "Water");
    when(priest.pickMessage()).thenReturn("a1d22n333a4444p").thenReturn("oaaawaala")
            .thenReturn("zmzmzmzaztzozh").thenReturn("Ahoy! Fight for me with men and money")
            .thenReturn("Do not waste paper");
    String ruler = this.chainBreaker.breakChain(competingKingdoms);

    Kingdom airKingdom = universe.getKingdomByName("Air");
    Kingdom iceKingdom = universe.getKingdomByName("Ice");
    Kingdom waterKingdom = universe.getKingdomByName("Water");
    assertThat(airKingdom.getAllies().size(), is(1));
    assertThat(iceKingdom.getAllies().size(), is(0));
    assertThat(waterKingdom.getAllies().size(), is(0));
  }

  @Test
  public void givenCompetingKingdoms_testShouldReturnTiesBetweenCompetitors() {
    List<String> competingKingdoms = List.of("Ice", "Fire");
    when(priest.pickMessage()).thenReturn("Octopus").thenReturn("Owl");
    String ruler = this.chainBreaker.breakChain(competingKingdoms);

    Kingdom iceKingdom = universe.getKingdomByName("Ice");
    Kingdom fireKingdom = universe.getKingdomByName("Fire");

    assertThat(iceKingdom.getAllies().size(), is(1));
    assertThat(fireKingdom.getAllies().size(), is(1));
  }
}
