package com.tameofthrones.app.model;

import com.tameofthrones.app.model.exception.InvalidKingdomNameException;
import org.junit.Test;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UniverseTest {

  private Universe universe = Universe.createUniverse();

  @Test
  public void testShouldVerifyKingdoms() {
    final String[] expectedKingdoms = {"Space", "Land", "Water", "Ice", "Air", "Fire"};
    List<String> actualKingdoms = universe.getKingdoms().stream().map(Kingdom::getKingdomName).collect(toList());
    assertThat(actualKingdoms.size(), is(expectedKingdoms.length));
    assertThat(actualKingdoms, containsInAnyOrder(expectedKingdoms));
  }

  @Test
  public void givenKingdomName_testShouldVerifyAndReturnTheKingdom() {
    Kingdom kingdom = universe.getKingdomByName("Space");
    assertThat(kingdom, notNullValue());
    assertThat(kingdom.getEmblem(), equalTo("Gorilla"));
    assertThat(kingdom.getKingdomName(), equalTo("Space"));
  }

  @Test
  public void givenKingdomNameInAnyCase_testShouldVerifyAndReturnTheKingdom() {
    Kingdom kingdom = universe.getKingdomByName("space");
    assertThat(kingdom, notNullValue());
    assertThat(kingdom.getEmblem(), equalTo("Gorilla"));
    assertThat(kingdom.getKingdomName(), equalTo("Space"));
  }

  @Test
  public void givenInvalidKingdomName_testShouldThrowInvalidKingdomNameException() {
    InvalidKingdomNameException kingdom = assertThrows(InvalidKingdomNameException.class,
            () -> universe.getKingdomByName("kingdom"));
    assertThat(kingdom, notNullValue());
    assertThat(kingdom.getMessage(), equalTo("Kingdom with name kingdom does not exist"));
  }
}
