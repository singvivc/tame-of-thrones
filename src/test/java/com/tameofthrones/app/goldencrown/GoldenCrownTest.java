package com.tameofthrones.app.goldencrown;

import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import com.tameofthrones.app.ruler.GoldenCrownRulerStrategy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.tameofthrones.app.model.Universe.NONE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GoldenCrownTest {

  private Universe universe = Universe.createUniverse();
  private GoldenCrown goldenCrown;

  @Before
  public void before() {
    this.goldenCrown = new GoldenCrown(new GoldenCrownRulerStrategy());
  }

  @After
  public void tearDown() {
    universe.getKingdoms().stream().forEach(kingdom -> kingdom.getAllies().clear());
  }

  @Test
  public void givenSecretMessages_testShouldVerifyNoKingdomSupportKingShan() {
    final String message = "secretMessage";
    final String ruler = goldenCrown.askForAllies(message);
    assertThat(ruler, equalTo(NONE));
  }

  @Test
  public void givenSecretMessage_testShouldVerifyShanBecomingTheRulerOrSoutheros() {
    final String message = "PandaOctopusMammoth";
    Kingdom shanKingdom = universe.getKingdomByName("space");

    final String ruler = goldenCrown.askForAllies(message);
    assertThat(ruler, equalTo(shanKingdom.getKing()));
    assertThat(shanKingdom.getAllies().size(), is(3));
  }
}
