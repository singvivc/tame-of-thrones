package com.tameofthrones.app.sender;

import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MessageSenderTest {

  private Universe universe = Universe.createUniverse();
  private MessageSender messageSender;

  @Before
  public void before() {
    this.messageSender = new MessageSender();
  }

  @Test
  public void givenReceiverAndTheMessage_testShouldReturnAcknowledged() {
    Kingdom to = universe.getKingdomByName("Space");
    boolean acknowledgment = messageSender.sendMessage(to, "Gorilla");
    assertThat(acknowledgment, is(true));
  }

  @Test
  public void givenReceiverAndTheMessage_testShouldReturnUnAcknowledged() {
    Kingdom to = universe.getKingdomByName("Air");
    boolean acknowledgment = messageSender.sendMessage(to, "Octopus");
    assertThat(acknowledgment, is(false));
  }
}
