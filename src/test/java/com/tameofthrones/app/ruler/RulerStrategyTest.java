package com.tameofthrones.app.ruler;

import org.junit.Test;

import java.util.NoSuchElementException;

import static com.tameofthrones.TestHelper.defaultKingdomsInSoutheros;
import static com.tameofthrones.TestHelper.getKingdomByName;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RulerStrategyTest {

  @Test
  public void givenKingdomWithAllies_testShouldVerifyKingdomWithAtleast3Allies() {
    var allKingdoms = defaultKingdomsInSoutheros();
    var spaceKingdom = getKingdomByName(allKingdoms,"Space");
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Air"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Water"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Fire"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Land"));

    RulerStrategy rulerStrategy = new GoldenCrownRulerStrategy();
    var optionalRuler = rulerStrategy.whoIsRulingSoutheros(allKingdoms);
    assertThat(optionalRuler, notNullValue());
    assertThat(optionalRuler.get(), notNullValue());
    assertThat(optionalRuler.get(), equalTo("King Shan"));
  }

  @Test
  public void givenAllKingdom_testShouldVerifyKingdomHasNoReverseSupport() {
    var allKingdoms = defaultKingdomsInSoutheros();
    var spaceKingdom = getKingdomByName(allKingdoms,"Space");
    var airKingdom = getKingdomByName(allKingdoms,"Air");

    spaceKingdom.makeAllies(airKingdom);
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Water"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Fire"));

    assertThat(spaceKingdom.getAllies().size(), is(3));
    assertThat(airKingdom, notNullValue());
    assertThat(airKingdom.getAllies().size(), is(0));
  }

  @Test
  public void givenMultipleKingdomsWithMaxAllies_testShouldVerifyAndReturnNoKingdom() {
    var allKingdoms = defaultKingdomsInSoutheros();
    var spaceKingdom = getKingdomByName(allKingdoms,"Space");
    var landKingdom = getKingdomByName(allKingdoms, "Land");

    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Air"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Fire"));
    landKingdom.makeAllies(getKingdomByName(allKingdoms,"Ice"));
    landKingdom.makeAllies(getKingdomByName(allKingdoms,"Water"));
    RulerStrategy rulerStrategy = new ChainBreakerRulerStrategy();
    var optionalRuler = rulerStrategy.whoIsRulingSoutheros(allKingdoms);
    assertThat(optionalRuler, notNullValue());
    assertThrows(NoSuchElementException.class, () -> optionalRuler.get());
  }

  @Test
  public void givenKingdomWithAllies_testShouldVerifyAndReturnKingdomWithMaxAllies() {
    var allKingdoms = defaultKingdomsInSoutheros();
    var spaceKingdom = getKingdomByName(allKingdoms,"Space");
    var landKingdom = getKingdomByName(allKingdoms, "Land");

    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Air"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Fire"));
    spaceKingdom.makeAllies(getKingdomByName(allKingdoms,"Ice"));
    landKingdom.makeAllies(getKingdomByName(allKingdoms,"Water"));

    RulerStrategy rulerStrategy = new ChainBreakerRulerStrategy();
    var optionalRuler = rulerStrategy.whoIsRulingSoutheros(allKingdoms);
    assertThat(optionalRuler, notNullValue());
    assertThat(optionalRuler.get(), equalTo("Space"));
  }
}
