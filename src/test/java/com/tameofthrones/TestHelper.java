package com.tameofthrones;

import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;

import java.util.List;
import java.util.Set;

import static com.tameofthrones.app.goldencrown.GoldenCrown.SHAN_KINGDOM;
import static com.tameofthrones.app.model.Universe.NONE;

public final class TestHelper {

  public TestHelper() {}

  public static Set<Kingdom> defaultKingdomsInSoutheros() {
    return Set.of(spaceKingdom(), landKingdom(), waterKingdom(), iceKingdom(), airKingdom(),
            fireKingdom());
  }

  public static void prompt(final String source, final Universe universe) {
    System.out.println("================== " + source + " ==================");
    System.out.println("Who is the ruler of Southeros?");
    System.out.println(universe.rulerOfSoutheros());
    System.out.println("Allies of Ruler?");
    Kingdom shanKingdom = universe.getKingdomByName(SHAN_KINGDOM);
    StringBuilder sb = new StringBuilder();
    if (shanKingdom.getAllies().size() >= 3) {
      shanKingdom.getAllies().stream().forEach(ally -> sb.append(ally.getKingdomName()).append(", "));
    }
    String allies = sb.toString().trim();
    System.out.println(allies.isBlank() ? NONE : allies.substring(0, allies.length() - 1));
  }

  public static Kingdom spaceKingdom() {
    return Kingdom.builder().kingdomName("Space").emblem("Gorilla").king("King Shan").build();
  }

  public static Kingdom landKingdom() {
    return Kingdom.builder().kingdomName("Land").emblem("Panda").build();
  }

  public static Kingdom waterKingdom() {
    return Kingdom.builder().kingdomName("Water").emblem("Octopus").build();
  }

  public static Kingdom iceKingdom() {
    return Kingdom.builder().kingdomName("Ice").emblem("Mammoth").build();
  }

  public static Kingdom airKingdom() {
    return Kingdom.builder().kingdomName("Air").emblem("Owl").build();
  }

  public static Kingdom fireKingdom() {
    return Kingdom.builder().kingdomName("Fire").emblem("Dragon").build();
  }

  public static Kingdom getKingdomByName(final Set<Kingdom> allKingdoms, final String kingdomName) {
    return allKingdoms.stream().filter(it -> it.getKingdomName().equalsIgnoreCase(kingdomName))
            .findFirst().get();
  }

  public static List<String> messagesInBallotBox() {
    return  List.of("Summer is coming", "a1d22n333a4444p", "oaaawaala",
            "zmzmzmzaztzozh", "Go, risk it all", "Let's swing the sword together",
            "Die or play the tame of thrones", "Ahoy! Fight for me with men and money",
            "Drag on Martin!",
            "When you play the tame of thrones, you win or you die.",
            "What could we say to the Lord of Death? Game on?",
            "Turn us away, and we will burn you first",
            "Death is so terribly final, while life is full of possibilities.",
            "You Win or You Die", "His watch is Ended",
            "Sphinx of black quartz, judge my dozen vows",
            "Fear cuts deeper than swords, My Lord.",
            "Different roads sometimes lead to the same castle.",
            "A DRAGON IS NOT A SLAVE.", "Do not waste paper",
            "Go ring all the bells",
            "Crazy Fredrick bought many very exquisite pearl, emerald and diamond jewels.",
            "The quick brown fox jumps over a lazy dog multiple times.",
            "We promptly judged antique ivory buckles for the next prize.",
            "Walar Morghulis: All men must die.");
  }

}
