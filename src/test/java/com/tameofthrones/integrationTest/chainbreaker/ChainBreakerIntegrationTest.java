package com.tameofthrones.integrationTest.chainbreaker;

import com.tameofthrones.TestHelper;
import com.tameofthrones.app.chainbreaker.ChainBreaker;
import com.tameofthrones.app.chainbreaker.model.Priest;
import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChainBreakerIntegrationTest {

  private Priest priest;
  private ChainBreaker chainBreaker;

  private Universe universe = Universe.createUniverse();

  @Before
  public void before() {
    priest = mock(Priest.class);
    chainBreaker = new ChainBreaker(priest);
    TestHelper.prompt("before", universe);
  }

  @After
  public void after() {
    TestHelper.prompt("after", universe);
    universe.getKingdoms().stream().forEach(kingdom -> kingdom.getAllies().clear());
  }

  @Test
  public void givenCompetingAllies_testShouldReturnTheRulerOfSoutheros() {
    when(priest.pickMessage()).thenReturn("a1d22n333a4444p").thenReturn("oaaawaala")
            .thenReturn("zmzmzmzaztzozh").thenReturn("Ahoy! Fight for me with men and money")
            .thenReturn("Do not waste paper");
    String ruler = chainBreaker.breakChain(List.of("Air", "Ice", "Water"));
    assertThat(ruler, equalTo("Air"));
    Kingdom airKingdom = universe.getKingdomByName("Air");
    assertThat(airKingdom.getAllies().size(), is(1));
  }

  @Test
  public void givenCompetingAllies_testShouldVerifyTiesBetweenKingdomsAndReVote() {
    when(priest.pickMessage()).thenReturn("Octopus").thenReturn("Gorilla");
    String ruler = chainBreaker.breakChain(List.of("Land", "Ice", "Air"));
    assertThat(ruler, equalTo("None"));
    Kingdom landKingdom = universe.getKingdomByName("Land");
    Kingdom iceKingdom = universe.getKingdomByName("Ice");

    assertThat(landKingdom.getAllies().size(), is(1));
    assertThat(iceKingdom.getAllies().size(), is(1));
    universe.getKingdoms().stream().forEach(kingdom -> kingdom.getAllies().clear());

    when(priest.pickMessage()).thenReturn("Octopus").thenReturn("Mammoth");
    ruler = chainBreaker.breakChain(List.of("Land", "Ice"));

    landKingdom = universe.getKingdomByName("Land");
    iceKingdom = universe.getKingdomByName("Ice");
    assertThat(landKingdom.getAllies().size(), is(1));
    assertThat(iceKingdom.getAllies().size(), is(0));
    assertThat(ruler, equalTo("Land"));
  }
}
