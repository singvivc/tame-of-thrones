package com.tameofthrones.integrationTest.goldencrown;

import com.tameofthrones.TestHelper;
import com.tameofthrones.app.goldencrown.GoldenCrown;
import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import com.tameofthrones.app.ruler.GoldenCrownRulerStrategy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.tameofthrones.app.goldencrown.GoldenCrown.SHAN_KINGDOM;
import static com.tameofthrones.app.model.Universe.NONE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GoldenCrownIntegrationTest {

  private Universe universe = Universe.createUniverse();
  private GoldenCrown goldenCrown;

  @Before
  public void before() {
    goldenCrown = new GoldenCrown(new GoldenCrownRulerStrategy());
    TestHelper.prompt("before", universe);
  }

  @After
  public void after() {
    TestHelper.prompt("after", universe);
    Kingdom shanKingdom = universe.getKingdomByName(SHAN_KINGDOM);
    shanKingdom.getAllies().clear();
  }

  @Test
  public void givenSecretMessage_testShouldVerifyNoKingdomSupportsSpaceKingdom() {
    final String ruler = goldenCrown.askForAllies("secretMessage");
    Kingdom shanKingdom = universe.getKingdomByName(SHAN_KINGDOM);
    assertThat(shanKingdom.getAllies().size(), is(0));
    assertThat(ruler, equalTo(NONE));
  }

  @Test
  public void givenValidSecretMessage_testShouldVerifyShanGettingSupportFromAirKingdomButIsNotTheRuler() {
    final String ruler = goldenCrown.askForAllies("Owl");

    Kingdom shanKingdom = universe.getKingdomByName(SHAN_KINGDOM);
    assertThat(shanKingdom.getAllies().size(), is(1));
    assertThat(ruler, equalTo(NONE));
  }

  @Test
  public void givenValidSecretMessageSendToAirLandAndWaterKingdom_kingShanShouldGetAllSupport() {
    final String ruler = goldenCrown.askForAllies("OwlPandaOctopus");

    Kingdom shanKingdom = universe.getKingdomByName(SHAN_KINGDOM);
    assertThat(shanKingdom.getAllies().size(), is(3));
    assertThat(ruler, equalTo("King Shan"));
  }
}
