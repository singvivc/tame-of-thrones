package com.tameofthrones.app.sender;

import com.tameofthrones.app.model.Kingdom;

import java.util.Map;

import static java.lang.Character.valueOf;
import static java.util.stream.Collectors.toMap;

public class MessageSender {

  public boolean sendMessage(final Kingdom to, final String secretMessage) {
    final String receiverEmblem = to.getEmblem();
    var secretCharFrequency = getCharacterFrequency(secretMessage);
    var receiverCharFrequency = getCharacterFrequency(receiverEmblem);

    boolean allMatch = receiverCharFrequency.keySet().stream()
            .allMatch(it -> receiverCharFrequency.get(it) <= secretCharFrequency.getOrDefault(it, 0));
    return allMatch;
  }

  public Map<Character, Integer> getCharacterFrequency(final String source) {
    return source.toLowerCase().chars().boxed()
            .collect(toMap(k -> valueOf((char) k.intValue()), v -> 1, Integer::sum));
  }
}
