package com.tameofthrones.app.ruler;

import com.tameofthrones.app.model.Kingdom;

import java.util.Optional;
import java.util.Set;

public interface RulerStrategy {

  public Optional<String> whoIsRulingSoutheros(final Set<Kingdom> kingdoms);
}
