package com.tameofthrones.app.ruler;

import com.tameofthrones.app.model.Kingdom;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;

public class ChainBreakerRulerStrategy implements RulerStrategy {

  @Override
  public Optional<String> whoIsRulingSoutheros(final Set<Kingdom> kingdoms) {
    var alliesSizeByKingdoms = kingdoms.stream().collect(groupingBy(kingdom -> kingdom.getAllies().size()));
    int max = alliesSizeByKingdoms.keySet().stream().max(Integer::compareTo).orElse(0);
    List<Kingdom> allies = alliesSizeByKingdoms.get(max);
    if (allies.size() > 1) {
      return Optional.empty();
    }
    Kingdom kingdom = allies.get(0);
    return Optional.of(kingdom.getKingdomName());
  }
}
