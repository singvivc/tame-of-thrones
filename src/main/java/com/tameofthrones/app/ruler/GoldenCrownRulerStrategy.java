package com.tameofthrones.app.ruler;

import com.tameofthrones.app.model.Kingdom;

import java.util.Optional;
import java.util.Set;

public class GoldenCrownRulerStrategy implements RulerStrategy {

  private final static int MAX_ALLIES_FOR_RULER = 3;

  @Override
  public Optional<String> whoIsRulingSoutheros(final Set<Kingdom> kingdoms) {
    var ruler = kingdoms.stream().filter(it -> it.getAllies().size() >= MAX_ALLIES_FOR_RULER).findFirst();
    return ruler.isPresent() ? Optional.of(ruler.get().getKing()) : Optional.empty();
  }
}
