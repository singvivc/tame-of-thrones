package com.tameofthrones.app.goldencrown;

import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import com.tameofthrones.app.ruler.RulerStrategy;
import com.tameofthrones.app.sender.MessageSender;

public class GoldenCrown {

  private final Universe universe = Universe.createUniverse();
  private final MessageSender messageSender = new MessageSender();

  public final static String SHAN_KINGDOM = "Space";
  private final Kingdom shanKingdom = universe.getKingdomByName(SHAN_KINGDOM);

  public GoldenCrown(final RulerStrategy rulerStrategy) {
    this.universe.setRulerStrategy(rulerStrategy);
  }

  public String askForAllies(final String secretMessage) {
    universe.getKingdoms().stream().filter(it -> !it.getKingdomName().equalsIgnoreCase(shanKingdom.getKingdomName()))
            .forEach(kingdom -> sendMessage(kingdom, secretMessage));
    return universe.rulerOfSoutheros();
  }

  private void sendMessage(final Kingdom to, final String secretMessage) {
    boolean acknowledgment = messageSender.sendMessage(to, secretMessage);
    if (acknowledgment) {
      shanKingdom.makeAllies(to);
    }
  }
}
