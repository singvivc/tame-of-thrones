package com.tameofthrones.app.chainbreaker.model;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Priest {

  private final Random random;
  private final List<String> ballotBox;

  public Priest(final List<String> ballotBox) {
    this.random = new Random();
    this.ballotBox = Collections.unmodifiableList(ballotBox);
  }

  public String pickMessage() {
    int index = this.random.nextInt(ballotBox.size());
    return ballotBox.get(index);
  }
}
