package com.tameofthrones.app.chainbreaker;

import com.tameofthrones.app.chainbreaker.model.Priest;
import com.tameofthrones.app.model.Kingdom;
import com.tameofthrones.app.model.Universe;
import com.tameofthrones.app.ruler.ChainBreakerRulerStrategy;
import com.tameofthrones.app.sender.MessageSender;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.tameofthrones.app.model.Universe.NONE;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class ChainBreaker {

  private final Priest priest;
  private final MessageSender messageSender = new MessageSender();
  private final Universe universe = Universe.createUniverse();
  private final Set<String> allegiance = new HashSet<>();

  public ChainBreaker(final Priest priest) {
    this.priest = priest;
    universe.setRulerStrategy(new ChainBreakerRulerStrategy());
  }

  public String breakChain(final List<String> competingKingdoms) {
    var kings = competingKingdoms.stream().map(universe::getKingdomByName).collect(toSet());
    var nonCompetingKingdoms = universe.getKingdoms().stream().filter(kingdom -> !kings.contains(kingdom))
            .collect(toList());
    if (nonCompetingKingdoms.isEmpty()) {
      return NONE;
    }
    allegiance.clear();
    return breakChain(kings, nonCompetingKingdoms);
  }

  private String breakChain(final Set<Kingdom> competingKings, final List<Kingdom> nonCompetingKingdoms) {
    competingKings.stream().forEach(kingdom -> {
      var message = priest.pickMessage();
      nonCompetingKingdoms.stream().forEach(receiver -> sendMessage(receiver, kingdom, message));
    });
    return universe.rulerOfSoutheros();
  }

  private void sendMessage(final Kingdom receiver, final Kingdom sender, final String message) {
    if (!allegiance.contains(receiver.getKingdomName())) {
      boolean acknowledgment = messageSender.sendMessage(receiver, message);
      if (acknowledgment) {
        sender.makeAllies(receiver);
        allegiance.add(receiver.getKingdomName());
      }
    }
  }
}
