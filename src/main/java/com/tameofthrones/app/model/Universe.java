package com.tameofthrones.app.model;

import com.tameofthrones.app.model.exception.InvalidKingdomNameException;
import com.tameofthrones.app.ruler.RulerStrategy;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
public final class Universe {

  public static final String NONE = "None";
  private static final Universe INSTANCE = new Universe();
  private final Set<Kingdom> kingdoms;
  @Setter
  private RulerStrategy rulerStrategy;

  private Universe() {
    this.kingdoms = Set.of(Kingdom.builder().kingdomName("Space").emblem("Gorilla").king("King Shan").build(),
            Kingdom.builder().kingdomName("Land").emblem("Panda").build(),
            Kingdom.builder().kingdomName("Water").emblem("Octopus").build(),
            Kingdom.builder().kingdomName("Ice").emblem("Mammoth").build(),
            Kingdom.builder().kingdomName("Air").emblem("Owl").build(),
            Kingdom.builder().kingdomName("Fire").emblem("Dragon").build());
  }

  public static Universe createUniverse() {
    return INSTANCE;
  }

  public Kingdom getKingdomByName(final String kingdomName) {
    return kingdoms.stream().filter(it -> it.getKingdomName().equalsIgnoreCase(kingdomName))
            .findFirst()
            .orElseThrow(() -> new InvalidKingdomNameException("Kingdom with name " + kingdomName + " does not exist"));
  }

  public String rulerOfSoutheros() {
    var optionalKingdom = rulerStrategy.whoIsRulingSoutheros(kingdoms);
    return optionalKingdom.isPresent() ? optionalKingdom.get() : NONE;
  }
}
