package com.tameofthrones.app.model.exception;

public class InvalidKingdomNameException extends RuntimeException {

  public InvalidKingdomNameException(final String errorMessage) {
    super(errorMessage);
  }
}
