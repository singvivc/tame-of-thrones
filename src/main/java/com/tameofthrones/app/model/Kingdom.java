package com.tameofthrones.app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Builder
@Getter
@ToString
@AllArgsConstructor
public class Kingdom {

  private final String king;
  private final String emblem;
  private final String kingdomName;

  @Builder.Default
  private final Set<Kingdom> allies = new HashSet<>();

  public void makeAllies(final Kingdom allies) {
    this.allies.add(allies);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emblem, kingdomName);
  }

  @Override
  public boolean equals(Object other) {
    Kingdom that = (Kingdom) other;
    return that.getKingdomName().equalsIgnoreCase(this.kingdomName)
            && that.getEmblem().equalsIgnoreCase(this.emblem);
  }
}
